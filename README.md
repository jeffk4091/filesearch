# FileSearch [![Release](https://jitpack.io/v/org.bitbucket.jeffk4091/filesearch.svg)](https://jitpack.io/#org.bitbucket.jeffk4091/filesearch)

A basic Java API and GUI for searching files, meant to be used in place of the standard JFileChooser.  FileSearch is able to search files by filename and file contents using a few different methods, and can be easily expanded to include additional search algorithms, including user defined algorithms.

###Add to my project

Using VERSION as the current version,

With Maven:

```xml
    <repositories>
        <repository>
            <id>jitpack.io</id>
            <url>https://jitpack.io</url>
        </repository>
    </repositories>

    <dependency>
        <groupId>org.bitbucket.kingj5</groupId>
        <artifactId>filesearch</artifactId>
        <version>VERSION</version>
    </dependency>
```

With Gradle:

```groovy
repositories {
    maven { url 'https://jitpack.io' }
}

dependencies {
    implementation 'org.bitbucket.kingj5:filesearch:VERSION'
}
```

###Cut to the chase, how do I use this?

The following example can be used to quickly instantiate the GUI within a dialog for use in any GUI based application:

```java
public class Example {
	public File createAndDisplayGui(File baseDir) {

		FileSearchPanelModel model = new DefaultFileSearchPanelModel(); //Create the base model
		model.setDirectory(baseDir); //Set the directory to default to searching in
		model.setSearchAlgorithm(new ThreadedRegexSearch(4)); //Can be substituted with any algorithm that implements the FileSearchAlgorithm interface

		FileSearchPanelController controller = new DefaultFileSearchPanelController(); //Create the controller
		controller.setModel(model); //Set the model in the Controller

		FileSearchPanel panel = new FileSearchPanel(); //Creates the FileSearch JPanel
		panel.setController(controller); //Set the controller in the panel
		panel.initialize(); //Initialize the panel, so that all subcomponents are placed within the panel.

		FileSearchDialog dialog = new FileSearchDialog(); //Creates a dialog that listens to events from the panel
		dialog.addView(panel); //Add the panel to the dialog
		dialog.setMinimumSize(new Dimension(800, 800)); //Set the size of the dialog
		dialog.setVisible(); //Make the dialog visible, thread will block on this call to wait for a file to be selected or not

		if(dialog.getFile() != null) {
			File selectedFile = dialog.getFile(); //If a file was selected, then set the selected file, and the file can be used by the application for whichever purpose
		}
	}
}
```

###The provided reference GUI and/or search algorithms don't satisfy my needs

FileSearch was coded to interfaces, so any component can be quickly swapped out with a different implementation if a change in functionality is desired.

A new search algorithm only needs to implement the FileSearchAlgorithm interface to be compatible with FileSearch.  It is recommended that any new algorithms extend the AbstractFileSearchAlgorithm class, which implements this interface and handles all of the boilerplate common code between all algorithms.

The Model, View and Controller can each be independently changed out if different GUI behavior is desired, and if desired, the FileSearchPanel can be added as a subpanel to an existing panel, or to a JFrame.  The class would need to register itself as a FileSearchPanelListener to receive feedback of if a file was selected and to retrieve that file.
