/**
 * Contains classes for implementing a model for the File Search suite.
 */
package org.bitbucket.kingj5.filesearch.model;