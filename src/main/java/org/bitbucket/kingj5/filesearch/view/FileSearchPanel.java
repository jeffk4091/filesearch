package org.bitbucket.kingj5.filesearch.view;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;

import org.bitbucket.kingj5.filesearch.controller.FileSearchPanelController;
import org.bitbucket.kingj5.filesearch.controller.FileSearchPanelControllerListener;
import org.bitbucket.kingj5.filesearch.search.FilteredFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A reference implementation of a File Search panel to be placed in a dialog.
 */
public class FileSearchPanel extends JPanel implements FileSearchPanelControllerListener {

	/**
	 * Controller to interface with to communicate with the model.
	 */
	private FileSearchPanelController controller;

	/**
	 * JList to hold the listed files
	 */
	private JList<FilteredFile> fileList;

	/**
	 * Open file button
	 */
	private JButton openButton;

	/**
	 * JComboBox to store the search directory
	 */
	private JComboBox<String> directoryBox;

	/**
	 * Search field, the text to search for will be entered here.
	 */
	private JTextField searchField;

	/**
	 * Search button, activates the search
	 */
	private JButton searchButton;

	/**
	 * Checkbox to toggle whether to search by title or not
	 */
	private JCheckBox searchFileName;

	/**
	 * Checkbox to toggle whether to search contents or not
	 */
	private JCheckBox searchContents;

	/**
	 * Checkbox to toggle whether to perform a case sensitive search or not
	 */
	private JCheckBox caseSensitive;

	/**
	 * Checkbox to toggle whether to perform a recursive search
	 */
	private JCheckBox recursiveSearch;

	/**
	 * ListModel for the listed files JList
	 */
	private DefaultListModel<FilteredFile> listModel;

	/**
	 * Used to store the contents of what is in the directory combobox, and compare to see if it has changed.
	 */
	private String boxContents;

	/**
	 * Boolean to indicate whether the dialog has been initialized or not
	 */
	private boolean initialized = false;

	/**
	 * Callback listener so that the panel may indicate to the parent frame or dialog that an option has been selected
	 * and that input should be passed to the implementing application and dialog closed.
	 */
	private FileSearchPanelListener listener;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * Registers the controller with the panel for updating the view
	 *
	 * @param controller Controller to be interacted with
	 */
	public void setController(FileSearchPanelController controller) {
		this.controller = controller;
		this.controller.addListener(this);
	}

	/**
	 * Registers a panel listener to receive events when a file has been selected or the cancel button is clicked
	 *
	 * @param listener Callback listener
	 */
	public void registerPanelListener(FileSearchPanelListener listener) {
		this.listener = listener;
	}

	/**
	 * Initializes the GUI components if they have not yet been initialized
	 */
	public void initialize() {
		if (!initialized) {
			setLayout(new BorderLayout());
			add(createDirectoryPanel(), BorderLayout.NORTH);
			add(createListPanel(), BorderLayout.CENTER);
			add(createSearchPanel(), BorderLayout.SOUTH);

			initialized = true;
		}
	}

	/**
	 * Generates the directory panel, containing the directory combobox and the browse button
	 *
	 * @return Directory panel
	 */
	private JPanel createDirectoryPanel() {
		JPanel panel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.weightx = 0.3;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.anchor = GridBagConstraints.EAST;
		gbc.fill = GridBagConstraints.HORIZONTAL;

		panel.add(new JLabel("Directory:"), gbc);

		gbc.gridx++;
		gbc.weightx = 0.7;
		this.directoryBox = new JComboBox<>();
		this.directoryBox.setEditable(false);
		boxContents = this.controller.getDirectoryAsFile().getAbsolutePath();
		this.directoryBox.addItem(boxContents);
		this.directoryBox.addActionListener(e -> handleChangeDirectory());
		panel.add(this.directoryBox, gbc);

		gbc.gridx++;
		gbc.weightx = 0.3;
		gbc.anchor = GridBagConstraints.NORTHEAST;

		JButton browseButton = new JButton("Browse");
		browseButton.addActionListener((e) -> handleBrowseDirectory());
		panel.add(browseButton, gbc);

		return panel;
	}

	/**
	 * Creates the JPanel containing the list of files in the directory.
	 *
	 * @return JPanel containing list of files
	 */
	private JPanel createListPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		this.fileList = new JList<>();
		this.fileList.setCellRenderer(new FilteredFileRenderer());
		this.fileList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		this.listModel = new DefaultListModel<>();
		this.fileList.setModel(this.listModel);

		for (FilteredFile f : this.controller.getListOfFiles()) {
			this.listModel.addElement(f);
		}

		this.fileList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					handleOpenFile();
				} else {
					JRootPane rootPane = SwingUtilities.getRootPane(openButton);
					rootPane.setDefaultButton(openButton);

				}
			}
		});

		JScrollPane scrollPane = new JScrollPane(this.fileList);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		panel.add(scrollPane, BorderLayout.CENTER);

		JPanel buttonPanel = new JPanel();

		openButton = new JButton("Open File");
		openButton.addActionListener((e) -> handleOpenFile());
		buttonPanel.add(openButton);

		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener((e) -> handleCancelAndClose());
		buttonPanel.add(cancelButton);

		panel.add(buttonPanel, BorderLayout.SOUTH);

		return panel;
	}

	/**
	 * Creates the search panel where options for what to search for may be defined.
	 *
	 * @return JPanel containing search options
	 */
	private JPanel createSearchPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(new TitledBorder("File Search"));

		JPanel searchPanel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.NORTHWEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 0.1;
		gbc.gridx = 0;
		gbc.gridy = 0;

		JLabel searchLabel = new JLabel("Search for: ");
		searchPanel.add(searchLabel, gbc);
		gbc.gridx++;
		gbc.weightx = 0.9;

		this.searchField = new JTextField();
		this.searchField.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				JRootPane rootPane = SwingUtilities.getRootPane(searchButton);
				rootPane.setDefaultButton(searchButton);
			}

			@Override
			public void focusLost(FocusEvent e) {

			}
		});
		searchPanel.add(this.searchField, gbc);
		gbc.gridx = 0;
		gbc.gridy++;

		gbc.weightx = 1;
		gbc.gridwidth = 3;

		JPanel buttonPanel = new JPanel();

		this.searchButton = new JButton("Search");
		this.searchButton.addActionListener((e) -> handleSearch());
		buttonPanel.add(this.searchButton);

		JButton cancelButton = new JButton("Cancel Search");
		cancelButton.addActionListener((e) -> handleCancelSearch());
		buttonPanel.add(cancelButton);

		JButton clearSearchButton = new JButton("Clear Search");
		clearSearchButton.setToolTipText("Restores the File List to the whole directory");
		clearSearchButton.addActionListener(e -> {
			this.searchField.setText("");
			directoryChanged();
		});
		buttonPanel.add(clearSearchButton);

		searchPanel.add(buttonPanel, gbc);

		panel.add(searchPanel, BorderLayout.CENTER);

		JPanel searchOptionsPanel = new JPanel(new GridLayout(2, 2));

		searchOptionsPanel.setBorder(new TitledBorder("Search Options"));

		this.searchFileName = new JCheckBox("Search Filename");
		this.searchFileName.setSelected(true);
		this.controller.setSearchFilename(true);
		this.searchFileName.addActionListener((e) -> handleSearchFileNameChecked());
		searchOptionsPanel.add(this.searchFileName);

		this.searchContents = new JCheckBox("Search File Contents");
		this.searchContents.setSelected(true);
		this.controller.setSearchFileContents(true);
		this.searchContents.addActionListener((e) -> handleSearchContentsChecked());
		searchOptionsPanel.add(this.searchContents);

		this.caseSensitive = new JCheckBox("Case Sensitive");
		this.caseSensitive.setSelected(false);
		this.controller.setCaseSensitive(false);
		this.caseSensitive.addActionListener(e -> handleCaseSensitiveChecked());
		searchOptionsPanel.add(this.caseSensitive);

		this.recursiveSearch = new JCheckBox("Recursive Search");
		this.recursiveSearch.setToolTipText(
				"Selecting this option will search both files in this directories and all files found in all directories found underneath this directory.");
		this.recursiveSearch.setSelected(false);
		this.controller.setRecursiveSearch(false);
		this.recursiveSearch.addActionListener(e -> handleRecursiveSearchChecked());
		searchOptionsPanel.add(this.recursiveSearch);

		panel.add(searchOptionsPanel, BorderLayout.EAST);

		return panel;
	}

	/**
	 * Handles the processing of the directory change, including validating that the directory has changed.
	 */
	private void handleChangeDirectory() {
		try {
			if (this.directoryBox.getSelectedItem() != null) {
				if (!this.directoryBox.getSelectedItem().equals(this.boxContents)) {
					File selectedFile = new File((String) this.directoryBox.getSelectedItem());
					if (selectedFile.isDirectory()) {
						if (selectedFile.exists()) {
							logger.debug("Directory to search changed to " + selectedFile.getAbsolutePath());
							this.controller.setDirectory(selectedFile);
							this.boxContents = selectedFile.getAbsolutePath();
						}
					}
				}
			}
		} catch (Exception e) {
			SwingUtilities.invokeLater(() -> {
				logger.error("File was invalid: " + e.getMessage(), e);
				JOptionPane.showMessageDialog(FileSearchPanel.this, "File was invalid: " + e.getMessage(), "Error",
						JOptionPane.ERROR_MESSAGE);
				this.directoryBox.setSelectedItem(boxContents);
			});
		}
	}

	/**
	 * Handles the setting of the search file contents property in the controller.
	 */
	private void handleSearchContentsChecked() {
		this.controller.setSearchFileContents(this.searchContents.isSelected());
	}

	/**
	 * Handles the setting of the search file name property in the controller.
	 */
	private void handleSearchFileNameChecked() {
		this.controller.setSearchFilename(this.searchFileName.isSelected());
	}

	/**
	 * Handles the setting of the case sensitive search property in the controller.
	 */
	private void handleCaseSensitiveChecked() {
		this.controller.setCaseSensitive(this.caseSensitive.isSelected());
	}

	/**
	 * Handles the setting of the recursive search property in teh controller.
	 */
	private void handleRecursiveSearchChecked() {
		this.controller.setRecursiveSearch(this.recursiveSearch.isSelected());
	}

	/**
	 * Handles executing the search.  Validates that a search term is present, communicates
	 * the search term to the controller and signals to the controller that the search should begin.
	 */
	private void handleSearch() {
		if (this.searchField.getText().trim().length() > 0) {
			this.controller.setSearchTerm(this.searchField.getText());
			this.searchButton.setEnabled(false);
			this.controller.runSearch();
		} else
			logger.error("User requested to search but no search term was entered.");
	}

	/**
	 * Handles cancelling the search.
	 */
	private void handleCancelSearch() {
		this.controller.cancelSearch();
		this.searchButton.setEnabled(true);
	}

	/**
	 * Handles communicating that a file has been selected to be opened to the registered listener
	 */
	private void handleOpenFile() {
		logger.info("User requested to open file " + this.fileList.getSelectedValue().getFile().getName());
		this.listener.handleFileSelected(this.fileList.getSelectedValue().getFile());
	}

	/**
	 * Handles the event when the cancel button in the panel is closed, rather than selecting a file to open
	 */
	private void handleCancelAndClose() {
		logger.debug("User requested to close GUI and not open a file");
		this.listener.handleCancelSelected();
	}

	/**
	 * Handles the event when the user wishes to change the search to use a different directory
	 */
	private void handleBrowseDirectory() {
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("Select Directory...");
		chooser.setFileHidingEnabled(false);
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setCurrentDirectory(this.controller.getDirectoryAsFile());
		chooser.setMultiSelectionEnabled(false);
		int returnValue = chooser.showOpenDialog(this);

		if (returnValue == JFileChooser.APPROVE_OPTION) {
			logger.debug("User selected to change directory to " + chooser.getSelectedFile().getAbsolutePath());
			if (!isFileInComboBox(chooser.getSelectedFile())) {
				logger.debug("Directory not in combobox, adding to available selections.");
				this.directoryBox.addItem(chooser.getSelectedFile().getAbsolutePath());
			}
			this.directoryBox.setSelectedItem(chooser.getSelectedFile().getAbsolutePath());
			this.controller.setDirectory(chooser.getSelectedFile());
		}
	}

	/**
	 * Checks if the selected file is already within the directory combo box
	 *
	 * @param file File to search for in the directory combo box
	 * @return True if it is in the combobox, false otherwise
	 */
	private boolean isFileInComboBox(File file) {
		int idx = this.directoryBox.getItemCount();
		String path = file.getAbsolutePath();
		for (int i = 0; i < idx; i++) {
			if (this.directoryBox.getItemAt(i).equals(path)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void searchCompleted() {
		SwingUtilities.invokeLater(() -> {
			this.listModel.removeAllElements();
			for (FilteredFile f : this.controller.getFilteredListOfFiles()) {
				this.listModel.addElement(f);
			}
			this.searchButton.setEnabled(true);
		});
	}

	@Override
	public void searchFailed(String message) {
		SwingUtilities.invokeLater(() -> {
			JOptionPane.showMessageDialog(FileSearchPanel.this, "Error during search:" + message, "Error during search",
					JOptionPane.ERROR_MESSAGE);
			this.searchButton.setEnabled(true);
		});

	}

	@Override
	public void directoryChanged() {
		SwingUtilities.invokeLater(() -> {
			this.listModel.removeAllElements();
			for (FilteredFile f : this.controller.getListOfFiles()) {
				this.listModel.addElement(f);
			}
		});
	}
}
