package org.bitbucket.kingj5.filesearch.view;

import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.border.EtchedBorder;

import org.bitbucket.kingj5.filesearch.search.FilteredFile;

/**
 * A Custom Cell Renderer for displaying a FilteredFile in a {@link JList}.
 * <p>
 * The Renderer shows the filename, along with a description, which ideally shows the text that was
 * found as a result of the search of the file.
 * </p>
 */
public class FilteredFileRenderer extends JPanel implements ListCellRenderer<FilteredFile> {

	/**
	 * JLabel containing the filenname of the file
	 */
	private JLabel filenameLabel = new JLabel();

	/**
	 * JLabel containing a description of what was found in the file
	 */
	private JLabel descriptionLabel = new JLabel();

	/**
	 * Constructor; creates the layout for the elements in the list.
	 */
	public FilteredFileRenderer() {
		super();

		Font labelFont = new Font("Dialog", Font.ITALIC, 11);
		descriptionLabel.setFont(labelFont);
		setLayout(new GridLayout(2, 1));
		setOpaque(true);
		add(filenameLabel);
		add(descriptionLabel);
		setBorder(new EtchedBorder());
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends FilteredFile> list, FilteredFile value, int index,
			boolean isSelected, boolean cellHasFocus) {

		filenameLabel.setText(value.getFile().getName());

		descriptionLabel.setText(value.getDescription());

		if (isSelected) {
			setBackground(list.getSelectionBackground());
			setForeground(list.getSelectionForeground());
		} else {
			setBackground(list.getBackground());
			setForeground(list.getForeground());
		}

		return this;
	}
}
