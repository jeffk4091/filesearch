package org.bitbucket.kingj5.filesearch.search;

import java.util.List;

/**
 * An interface defining a FileSearchAlgorithm.  Any search algorithms that will interact with FileSearch must implement this interface.
 * <p>
 * It is recommended that any newly developed algorithms extend {@link AbstractFileSearchAlgorithm} as that class
 * defines much of the boilerplate code that would be repeated.
 * </p>
 */
public interface FileSearchAlgorithm {

	/**
	 * Registers a callback listener to receive signal that a search has completed
	 *
	 * @param listener Callback listener
	 */
	void addListener(FileSearchListener listener);

	/**
	 * Removes a previously registered listener
	 *
	 * @param listener Listener to be unregistered.
	 */
	void removeListener(FileSearchListener listener);

	/**
	 * Sets the list of files to be searched
	 *
	 * @param files The list of files to be searched.
	 */
	void setListOfFiles(List<FilteredFile> files);

	/**
	 * Gets the list of files that have been filtered as a result of the search
	 *
	 * @return The list of filtered files
	 */
	List<FilteredFile> getListOfFilteredFiles();

	/**
	 * Set the search string
	 *
	 * @param searchTerm String containing what to search for
	 */
	void setSearchTerm(String searchTerm);

	/**
	 * Gets the search term
	 *
	 * @return Search term
	 */
	String getSearchTerm();

	/**
	 * Sets the search filename property
	 *
	 * @param search True if file]names should be searched, false otherwise
	 */
	void setSearchFilename(boolean search);

	/**
	 * Sets the search file contents property
	 *
	 * @param search True if file contents should be searched, false otherwise
	 */
	void setSearchFileContents(boolean search);

	/**
	 * Sets the case sensitive search property
	 *
	 * @param caseSensitive True if a search should be case sensitive, false otherwise
	 */
	void setCaseSensitive(boolean caseSensitive);

	/**
	 * Sets the recursive search property
	 *
	 * @param recursiveSearch True if search should be recursive, false otherwise
	 */
	void setRecursiveSearch(boolean recursiveSearch);

	/**
	 * Execute the search
	 */
	void runSearch();

	/**
	 * Cancel a currently running search
	 */
	void cancelSearch();
}
