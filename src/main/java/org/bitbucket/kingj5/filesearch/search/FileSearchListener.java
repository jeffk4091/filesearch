package org.bitbucket.kingj5.filesearch.search;

/**
 * A callback interface to alert the model that a search has completed or errored
 */
public interface FileSearchListener {

	/**
	 * Notify the listener that the search has completed
	 */
	void searchCompleted();

	/**
	 * Notify the listener that the search has failed with the given error message.
	 *
	 * @param message Error returned by the search
	 */
	void searchError(String message);
}
