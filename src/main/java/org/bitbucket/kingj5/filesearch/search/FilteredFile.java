package org.bitbucket.kingj5.filesearch.search;

import java.io.File;

/**
 * A FilteredFile is a simple data structure containing fields for holding a file and a description.
 * <p>
 * This data structure is used for holding file search results.
 */
public class FilteredFile {

	/**
	 * Immutable reference to a searched file
	 */
	private final File file;

	/**
	 * Immutable description of what was found in the file
	 */
	private final String description;

	/**
	 * Create a filtered file with the given file and description
	 *
	 * @param file        File to be used
	 * @param description Description to be used
	 */
	public FilteredFile(File file, String description) {
		this.file = file;
		this.description = description;
	}

	/**
	 * Returns the file contained within the FilteredFile
	 *
	 * @return File that was searched
	 */
	public File getFile() {
		return file;
	}

	/**
	 * Returns the description of what was found within the FilteredFile
	 *
	 * @return Description of what was in the Filtered File
	 */
	public String getDescription() {
		return description;
	}

}
