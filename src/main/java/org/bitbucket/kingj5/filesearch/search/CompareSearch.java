package org.bitbucket.kingj5.filesearch.search;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A basic compare search algorithm.  This uses {@link String#contains(CharSequence)}
 * to search file names and contents for the given search term.
 */
public class CompareSearch extends AbstractFileSearchAlgorithm {

	/**
	 * Logger for class
	 */
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * Executes the search.  The CompareSearch uses {@link String#contains(CharSequence)}
	 * to search for the given search term in the given files.  If searches will not
	 * be case sensitive, the search term and the contents being searched are
	 * converted to lowercase prior to being searched.
	 */
	@Override
	public void runSearch() {
		intermediateFiles.clear();
		getListOfFilteredFiles().clear();

		cancelSearch = false;
		try {
			logger.info("Executing search via CompareSearch");
			for (File file : getListOfFiles()) {
				if (cancelSearch)
					return;
				if (isSearchFilename()) {
					searchFileName(file);
				}
				if (isSearchFileContents()) {
					searchFileContents(file);
				}
			}
		} catch (Exception e) {
			logger.error("Error encountered during search: " + e.getMessage(), e);
			notifySearchError(e.getMessage());
			return;
		}
		logger.debug("Search completed");

		finalizeFileList();

		notifySearchCompleted();

	}

	/**
	 * Searches the filename of the given file to see if it contains the search term.
	 *
	 * @param file File to be searched
	 */
	private void searchFileName(File file) {
		String fileName = file.getName();
		logger.trace("Searching filename [" + fileName + "] for search term [" + getSearchTerm() + "]");
		if (!isCaseSensitive()) {
			if (fileName.toLowerCase().contains(getSearchTerm().toLowerCase())) {
				logger.trace("Search matched for filename [" + fileName + "], adding to intermediate list.");
				intermediateFiles.add(file);
			}
		} else {
			if (fileName.contains(getSearchTerm())) {
				logger.trace("Search matched for filename [" + fileName + "], adding to intermediate list.");
				intermediateFiles.add(file);
			}
		}

	}

	/**
	 * Searches the file contents of the given file to see if it contains the search term.
	 * This algorithm reads a file line by line and will compare against the entire line.
	 * If a match is found, searching is suspended for the file and will not continue to
	 * read the rest of the file as that is redundant.
	 *
	 * @param file
	 * @throws Exception
	 */
	private void searchFileContents(File file) throws Exception {
		logger.trace("Searching file contents of [" + file.getName() + "] for [" + getSearchTerm() + "]");
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			String line = reader.readLine();
			while (line != null) {
				logger.trace("Read line of file.");
				if (!isCaseSensitive()) {
					if (line.toLowerCase().contains(getSearchTerm().toLowerCase())) {
						logger.debug("Match found in file, creating FilteredFile");
						createFilteredFile(file, line);
						break;
					}
				} else {
					if (line.contains(getSearchTerm())) {
						logger.debug("Match found in file, creating FilteredFile");
						createFilteredFile(file, line);
						break;
					}
				}
				logger.trace("Match not found in current line, reading next line.");
				line = reader.readLine();
			}

		}
	}

	/**
	 * Creates a filtered file using the given file and line.  If it is contained within the
	 * intermediate files list, it is removed from that list
	 *
	 * @param file File being searched
	 * @param line Description of what was found in the file
	 */
	private void createFilteredFile(File file, String line) {
		if (intermediateFiles.contains(file))
			intermediateFiles.remove(file);
		FilteredFile filteredFile = new FilteredFile(file, createDescription(line));
		getListOfFilteredFiles().add(filteredFile);
	}

	/**
	 * Creates a description of what was found using the line that was found and the search term
	 *
	 * @param line Line containing the search match
	 * @return Description to be included in the {@link FilteredFile}
	 */
	private String createDescription(String line) {
		int beginIndex = line.indexOf(getSearchTerm());
		int endIndex = beginIndex + getSearchTerm().length();
		int descriptionBegin;
		StringBuilder sb = new StringBuilder();

		if (beginIndex < 30) {
			descriptionBegin = 0;
		} else {
			descriptionBegin = beginIndex - 30;
			sb.append("...");
		}

		if (line.substring(endIndex).length() < 30) {
			sb.append(line.substring(descriptionBegin));
		} else {
			sb.append(line.substring(descriptionBegin, (endIndex + 30)));
			sb.append("...");
		}

		return sb.toString();
	}

}
