/**
 * <p>The File Search API is a library aimed at being used for searching files using a graphical interface.</p>
 *
 * <p>
 * This API includes a number of search algorithms that can be used for
 * searching a directory of files for user specified content, along with a
 * {@link org.bitbucket.kingj5.filesearch.search.FileSearchAlgorithm}
 * that can be used to make your own search algorithm.
 * </p>
 * <p>
 * It also provides a model for creating a File Search dialog, similar to a
 * {@link javax.swing.JFileChooser} and a reference implementation that
 * can be quickly implemented by a GUI application.
 * </p>
 */
package org.bitbucket.kingj5.filesearch;