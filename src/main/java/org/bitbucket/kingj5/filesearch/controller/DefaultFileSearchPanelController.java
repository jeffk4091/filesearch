package org.bitbucket.kingj5.filesearch.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bitbucket.kingj5.filesearch.model.FileSearchPanelModel;
import org.bitbucket.kingj5.filesearch.model.FileSearchPanelModelListener;
import org.bitbucket.kingj5.filesearch.search.FilteredFile;

/**
 * A default implementation of the {@link FileSearchPanelController} interface which is
 * used in the reference implementation of FileSearch
 */
public class DefaultFileSearchPanelController implements FileSearchPanelController, FileSearchPanelModelListener {

	/**
	 * Model to interact with
	 */
	private FileSearchPanelModel model;

	/**
	 * List of callback listeners to interact with
	 */
	private List<FileSearchPanelControllerListener> listeners = new ArrayList<>();

	@Override
	public void setModel(FileSearchPanelModel model) {
		this.model = model;
		this.model.addListener(this);
	}

	@Override
	public void addListener(FileSearchPanelControllerListener listener) {
		synchronized (this.listeners) {
			this.listeners.add(listener);
		}
	}

	@Override
	public void removeListener(FileSearchPanelControllerListener listener) {
		synchronized (this.listeners) {
			this.listeners.remove(listener);
		}
	}

	@Override
	public void setDirectory(File directory) {
		this.model.setDirectory(directory);
	}

	@Override
	public void setDirectory(String directory) {
		this.model.setDirectory(directory);
	}

	@Override
	public String getDirectoryAsString() {
		return this.model.getDirectoryAsString();
	}

	@Override
	public File getDirectoryAsFile() {
		return this.model.getDirectoryAsFile();
	}

	@Override
	public void setSearchTerm(String searchTerm) {
		this.model.setSearchTerm(searchTerm);
	}

	@Override
	public List<FilteredFile> getListOfFiles() {
		return this.model.getListOfFiles();
	}

	@Override
	public List<FilteredFile> getFilteredListOfFiles() {
		return this.model.getFilteredListOfFiles();
	}

	@Override
	public void setSearchFilename(boolean search) {
		this.model.setSearchFilename(search);
	}

	@Override
	public boolean getSearchFilename() {
		return this.model.getSearchFilename();
	}

	@Override
	public void setSearchFileContents(boolean search) {
		this.model.setSearchFileContents(search);
	}

	@Override
	public boolean getSearchFileContents() {
		return this.model.getSearchFileContents();
	}

	@Override
	public void setCaseSensitive(boolean caseSensitive) {
		this.model.setCaseSensitive(caseSensitive);
	}

	@Override
	public boolean getCaseSensitive() {
		return this.model.getCaseSensitive();
	}

	@Override
	public void setRecursiveSearch(boolean recursiveSearch) {
		this.model.setRecursiveSearch(recursiveSearch);
	}

	@Override
	public boolean getRecursiveSearch() {
		return this.model.getRecursiveSearch();
	}

	@Override
	public void runSearch() {
		this.model.runSearch();
	}

	@Override
	public void cancelSearch() {
		this.model.cancelSearch();
	}

	@Override
	public void searchCompleted() {
		synchronized (this.listeners) {
			for (FileSearchPanelControllerListener l : listeners) {
				l.searchCompleted();
			}
		}
	}

	@Override
	public void searchFailed(String message) {
		synchronized (this.listeners) {
			for (FileSearchPanelControllerListener l : listeners) {
				l.searchFailed(message);
			}
		}
	}

	@Override
	public void directoryChanged() {
		synchronized (this.listeners) {
			for (FileSearchPanelControllerListener l : listeners) {
				l.directoryChanged();
			}
		}
	}
}
