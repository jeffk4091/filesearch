package org.bitbucket.kingj5.filesearch.controller;

/**
 * Provides an interface so a class can be registered to the controller for callbacks
 */
public interface FileSearchPanelControllerListener {

	/**
	 * Callback method for when search has been completed
	 */
	void searchCompleted();

	/**
	 * Callback method for when a search has failed, including a string as to why
	 *
	 * @param message Message containing the error that was encountered
	 */
	void searchFailed(String message);

	/**
	 * Callback method for when the directory listing has been populated so an updated view can be displayed
	 */
	void directoryChanged();

}
