/**
 * Contains classes for implementing a controller for the File Search suite.
 */
package org.bitbucket.kingj5.filesearch.controller;