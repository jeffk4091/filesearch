import java.awt.Dimension;
import java.io.File;

import org.bitbucket.kingj5.filesearch.controller.DefaultFileSearchPanelController;
import org.bitbucket.kingj5.filesearch.controller.FileSearchPanelController;
import org.bitbucket.kingj5.filesearch.model.DefaultFileSearchPanelModel;
import org.bitbucket.kingj5.filesearch.model.FileSearchPanelModel;
import org.bitbucket.kingj5.filesearch.search.ThreadedRegexSearch;
import org.bitbucket.kingj5.filesearch.view.FileSearchDialog;
import org.bitbucket.kingj5.filesearch.view.FileSearchPanel;

public class LaunchDialog {

	public static void main(String[] args)
	{
		if (args.length > 0) {

			FileSearchPanelModel model = new DefaultFileSearchPanelModel();
			model.setDirectory(new File(args[0]));
			model.setSearchAlgorithm(new ThreadedRegexSearch(4));
			FileSearchPanelController controller = new DefaultFileSearchPanelController();
			controller.setModel(model);
			FileSearchPanel panel = new FileSearchPanel();
			panel.setController(controller);
			panel.initialize();

			FileSearchDialog dialog = new FileSearchDialog();
			dialog.addView(panel);
			dialog.setMinimumSize(new Dimension(800, 800));
			dialog.setVisible();
		}
	}
}
